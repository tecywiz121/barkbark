package main

import (
	"log"
	"fmt"
	"net"
	"strings"
	"strconv"
	"time"
	
	"github.com/nanu-c/qml-go"
)

type TestStruct struct {
	Root    qml.Object
	Message string
	Node ChatNode
}

type ChatNode struct {
	peers map[string]*Peer
	sock *net.UDPConn
	sentMsgs []sentMsg
}

type sentMsg struct {
	sent time.Time
	text string
}

type Peer struct {
	ip *net.UDPAddr
	noRecv int64
	saidSent int64
}

func main() {
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	engine := qml.NewEngine()
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}
	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}
	
	conn, err := net.ListenMulticastUDP("udp", nil, addr)
	if err != nil {
		log.Fatal(err)
	}

	testvar := TestStruct{
		Message: "",
		Node: ChatNode {
			peers: make(map[string]*Peer),
			sock: conn,
		},
	}

	go testvar.Node.sendHellos()

	context := engine.Context()
	context.SetVar("testvar", &testvar)

	win := component.CreateWindow(nil)
	testvar.Root = win.Root()
	win.Show()
	win.Wait()

	return nil
}

func (testvar *TestStruct) Start() {
	go testvar.Node.receiver(testvar)
}

func (testvar *TestStruct) Send(text string) {
	go testvar.Node.sendText(text)
}

func (this *ChatNode) sendText(text string) {
	now := time.Now()
	toSend := sentMsg {
		sent: now,
		text: text,
	}
	this.sentMsgs = append(this.sentMsgs, toSend)
	this.sendTextAt(len(this.sentMsgs) - 1)
}

func (this *ChatNode) sendTextAt(index int) {
	toSend := this.sentMsgs[index]
	msg := fmt.Sprintf("t|%d|%s|%s", index + 1, toSend.sent.Format(time.RFC3339), toSend.text)
	this.write(msg)
	log.Printf("Sending Text")
}

func (this *ChatNode) sendHellos() {
	timer := time.NewTicker(30*time.Second)

	for true {
		this.sendHello()
		<- timer.C
	}
}

func (this *ChatNode) sendHello() {
	now := time.Now().Format(time.RFC3339)
	msg := fmt.Sprintf("h|%d|%s", len(this.sentMsgs), now)
	this.write(msg)
	log.Printf("Sending hello")
}

func (this *ChatNode) sendEnumerate(addr *net.UDPAddr) {
	peer := this.peers[addr.String()]
	msg := fmt.Sprintf("e|%d", peer.noRecv + 1)
	this.writeTo(addr, msg)
	log.Printf("Sending enumeration")
}

func (this *ChatNode) write(msg string) {
	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}
	this.writeTo(addr, msg)
}

func (this *ChatNode) writeTo(addr *net.UDPAddr, msg string) {
	buff := []byte(msg)
	_, _, err := this.sock.WriteMsgUDP(buff, nil, addr)
	if err != nil {
		log.Fatal(err)
	}
}

func (this *ChatNode) receiver(testvar *TestStruct) {
	var buffer[65000] byte
	var oob[0] byte

	for true {
		size, _, _,from, err:= this.sock.ReadMsgUDP(buffer[:], oob[:])
		if err != nil {
			log.Fatal(err)
		}

		if from == this.sock.LocalAddr() {
			continue
		}

		bytes := buffer[:size]
		msg := string (bytes)
		if strings.HasSuffix(msg, "\n") {
			msg = msg[:size-1]
		}

		testvar.Message = msg
		qml.Changed(testvar, &testvar.Message)
		log.Print("Got it chief %s", msg)

		switch msg[0] {
		case 't':
			handleText(testvar, from, msg)
		case 'h':
			handleHello(testvar, from, msg)
		case 'e':
			handleEnumerate(testvar, from, msg)
		}
	}
}

func handleText(testvar *TestStruct, ip *net.UDPAddr, msg string) {
	parts := strings.SplitN(msg, "|", 4)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	_, err = time.Parse(time.RFC3339, parts[2])
	if err != nil {
		log.Fatal(err)
	}
	testvar.Node.onTextMsg(ip, id, parts[3])
}

func handleHello(testvar *TestStruct, from *net.UDPAddr, msg string) {
	parts := strings.SplitN(msg, "|", 3)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	_, err = time.Parse(time.RFC3339, parts[2])
	if err != nil {
		log.Fatal(err)
	}
	testvar.Node.onHelloMsg(from, id)
}

func handleEnumerate(testvar *TestStruct, from *net.UDPAddr, msg string) {
	log.Printf("%v Requested enumerate from %s", from, msg)
	
	parts := strings.SplitN(msg, "|", 2)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	testvar.Node.onEnumerateMsg(from, id)
}

func (this *ChatNode) onEnumerateMsg(ip *net.UDPAddr, id int64) {
	for ii := int(id)-1; ii < len(this.sentMsgs); ii ++ {
		this.sendTextAt(ii)
	} 
}

func (this *ChatNode) onHelloMsg(ip *net.UDPAddr, saidSent int64) {
	if this.peers[ip.String()] == nil {
		this.peers[ip.String()] = &Peer {
			ip:ip,
			noRecv:0,
			saidSent:0,
		}
	}
	this.peers[ip.String()].saidSent = saidSent
	log.Printf("%v Has sent %d", ip, saidSent)
	noRecv := this.peers[ip.String()].noRecv
	
	if saidSent <= noRecv {
	return
	}

this.peers[ip.String()].saidSent	= saidSent
this.sendEnumerate(ip)
}

func (this *ChatNode) onTextMsg(ip *net.UDPAddr, id int64, text string) {
	
	if this.peers[ip.String()] == nil {
		log.Printf("New peer %v", ip)
		this.peers[ip.String()] = &Peer {
			ip:ip,
			noRecv:0,
			saidSent:0,
		}
	}
	
	noRecv := this.peers[ip.String()].noRecv
	log.Printf("%v Sent txt msg with id %d", ip, id)
	
	if id <= noRecv {
	} else if id - noRecv == 1 {
		this.peers[ip.String()].noRecv += 1
		log.Printf("[%v]: %s", ip, text) 
	} else {
	this.peers[ip.String()].saidSent	= id
	this.sendEnumerate(ip)
	}
}